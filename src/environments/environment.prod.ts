export const environment = {
  production: true,
  hmr: false,
  apiUrl: 'http://localhost:3000/api'
}
