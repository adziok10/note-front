import { User } from '../../modules/user/typings/User'

export interface Notebook {
    _id: string
    name: string
    description: string
    ownerId: User['_id']
    color: string
}
