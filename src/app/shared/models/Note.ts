import { Notebook } from './Notebook'

export interface Note {
    _id: string
    content: string
    title: string
    notebookId: Notebook['_id']
}
