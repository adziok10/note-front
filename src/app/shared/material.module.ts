import { NgModule } from '@angular/core'
import { HAMMER_GESTURE_CONFIG } from '@angular/platform-browser'
import { GestureConfig } from '@angular/material/core';
import 'hammerjs'

import { MatAutocompleteModule } from '@angular/material/autocomplete'
import { MatButtonModule } from '@angular/material/button'
import { MatCardModule } from '@angular/material/card'
import { MatCheckboxModule } from '@angular/material/checkbox'
import { MatChipsModule } from '@angular/material/chips'
import { MatNativeDateModule } from '@angular/material/core'
import { MatDatepickerModule } from '@angular/material/datepicker'
import { MatExpansionModule } from '@angular/material/expansion'
import { MatIconModule } from '@angular/material/icon'
import { MatInputModule } from '@angular/material/input'
import { MatListModule } from '@angular/material/list'
import { MatMenuModule } from '@angular/material/menu'
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar'
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner'
import { MatSelectModule } from '@angular/material/select'
import { MatSidenavModule } from '@angular/material/sidenav'
import { MatSlideToggleModule } from '@angular/material/slide-toggle'
import { MatSliderModule } from '@angular/material/slider'
import { MatSnackBarModule } from '@angular/material/snack-bar'
import { MatStepperModule } from '@angular/material/stepper'
import { MatTableModule } from '@angular/material/table'
import { MatTabsModule } from '@angular/material/tabs'
import { MatTooltipModule } from '@angular/material/tooltip'
import { MatDialogModule } from '@angular/material/dialog'

@NgModule({
    imports: [
        MatInputModule,
        MatSelectModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatButtonModule,
        MatCardModule,
        MatTableModule,
        MatExpansionModule,
        MatCheckboxModule,
        MatListModule,
        MatMenuModule,
        MatPaginatorModule,
        MatTabsModule,
        MatProgressSpinnerModule,
        MatSidenavModule,
        MatIconModule,
        MatSnackBarModule,
        MatProgressBarModule,
        MatChipsModule,
        MatAutocompleteModule,
        MatSliderModule,
        MatTooltipModule,
        MatStepperModule,
        MatSlideToggleModule,
        MatDialogModule
    ],
    exports: [
        MatInputModule,
        MatSelectModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatButtonModule,
        MatCardModule,
        MatTableModule,
        MatExpansionModule,
        MatCheckboxModule,
        MatListModule,
        MatMenuModule,
        MatPaginatorModule,
        MatTabsModule,
        MatProgressSpinnerModule,
        MatSidenavModule,
        MatIconModule,
        MatSnackBarModule,
        MatProgressBarModule,
        MatChipsModule,
        MatAutocompleteModule,
        MatSliderModule,
        MatTooltipModule,
        MatStepperModule,
        MatSlideToggleModule,
        MatDialogModule
    ],
    providers: [
        { provide: HAMMER_GESTURE_CONFIG, useClass: GestureConfig }
    ],
})
export class MaterialModule { }
