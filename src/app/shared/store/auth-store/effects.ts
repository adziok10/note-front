import { Injectable } from '@angular/core'
import { Router } from '@angular/router'
import { Actions, Effect, ofType } from '@ngrx/effects'
import { Action } from '@ngrx/store'
import { Observable, of } from 'rxjs'
import { catchError, map, startWith, switchMap, concatMap } from 'rxjs/operators'
import { tap } from 'rxjs/operators'

import {
    AuthActionTypes,
    LogInRequestAction,
    LogInSuccessAction,
    LogInFailureAction,
    LogOutAction,
    StatusFailureAction,
    StatusRequestAction,
    StatusSuccessAction,
    RegisterSuccessAction,
    RegisterFailureAction
} from './actions'
import { UserService } from '../../services/user.service'
import { User } from '../../../modules/user/typings/User'

@Injectable()
export class AuthEffects {
    constructor(
        private userService: UserService,
        private actions: Actions,
        private router: Router,
    ) { }

    @Effect()
    LogIn: Observable<any> = this.actions.pipe(
        ofType(AuthActionTypes.LOGIN_REQUEST),
        concatMap((payload: any) => {
            return this.userService.logIn({ email: payload.payload.email, password: payload.payload.password })
            .pipe(
                map((user: User) => new LogInSuccessAction({ token: user.token, email: user.email, _id: user._id })),
                catchError(error => of(new LogInFailureAction({ error: error })))
            )
        }))

    @Effect({ dispatch: false })
    LogInSuccess: Observable<any> = this.actions.pipe(
        ofType(AuthActionTypes.LOGIN_SUCCESS),
        tap((user) => {
            localStorage.setItem('token', user.payload.token)
            this.router.navigateByUrl('/')
        })
    )

    @Effect({ dispatch: false })
    LogInFailure: Observable<any> = this.actions.pipe(
        ofType(AuthActionTypes.LOGIN_FAILURE)
    )

    @Effect()
    Register: Observable<any> = this.actions.pipe(
        ofType(AuthActionTypes.REGISTER_REQUEST),
        concatMap((payload: any) => {
            return this.userService.register({ email: payload.payload.email, password: payload.payload.password, login: payload.payload.login })
            .pipe(
                map((user: User) => new RegisterSuccessAction({})),
                catchError(error => of(new RegisterFailureAction({})))
            )
        }))

    @Effect({ dispatch: false })
    RegisterSuccess: Observable<any> = this.actions.pipe(
        ofType(AuthActionTypes.REGISTER_SUCCESS),
        tap((user) => {
            this.router.navigateByUrl('/')
        })
    )

    @Effect({ dispatch: false })
    RegisterFailure: Observable<any> = this.actions.pipe(
        ofType(AuthActionTypes.REGISTER_FAILURE)
    )

    @Effect({ dispatch: false })
    LogOut: Observable<any> = this.actions.pipe(
        ofType(AuthActionTypes.LOGOUT),
        tap((user) => {
            localStorage.removeItem('token')
            this.router.navigateByUrl('/')
        })
    )

    @Effect()
    Status: Observable<any> = this.actions.pipe(
        ofType(AuthActionTypes.STATUS_REQUEST),
        concatMap((payload: any) => {
            return this.userService.checkStatus()
            .pipe(
                map((user: User) => new StatusSuccessAction({ token: user.token, email: user.email, _id: user._id })),
                catchError(error => of(new StatusFailureAction({ error: error })))
            )
        }))

    @Effect({ dispatch: false })
    StatusSuccess: Observable<any> = this.actions.pipe(
        ofType(AuthActionTypes.STATUS_SUCCESS),
        tap((user) => {
            localStorage.setItem('token', user.payload.token)
        })
    )

    @Effect({ dispatch: false })
    StatusFailure: Observable<any> = this.actions.pipe(
        ofType(AuthActionTypes.STATUS_FAILURE),
        tap(() => {
            // localStorage.removeItem('token')
            // this.router.navigateByUrl('/')
        })
    )
}
