import { AuthActionTypes, AuthAll } from './actions'
import { initialState, AuthState } from './state'

export function authReducers(state = initialState, action: AuthAll): AuthState {

    switch (action.type) {
        case AuthActionTypes.LOGIN_REQUEST: {
            return {
                ...state,
                isAuthenticated: false,
                error: null,
                loading: true,
            }
        }
        case AuthActionTypes.LOGIN_SUCCESS: {
            return {
                ...state,
                isAuthenticated: true,
                user: {
                  token: action.payload.token,
                  email: action.payload.email,
                  _id: action.payload._id
                },
                error: null,
                loading: false,
              }
        }
        case AuthActionTypes.LOGIN_FAILURE: {
            return {
                ...state,
                isAuthenticated: false,
                error: action.payload.error || 'error',
                loading: false,
            }
        }

        case AuthActionTypes.REGISTER_REQUEST: {
            return {
                ...state,
                isAuthenticated: false,
                error: null,
                loading: true,
            }
        }
        case AuthActionTypes.REGISTER_SUCCESS: {
            return {
                ...state,
                isAuthenticated: false,
                error: null,
                loading: false,
              }
        }
        case AuthActionTypes.REGISTER_FAILURE: {
            return {
                ...state,
                isAuthenticated: false,
                error: action.payload.error || 'error',
                loading: false,
            }
        }

        case AuthActionTypes.LOGOUT: {
            return {
                ...state,
                isAuthenticated: false,
                loading: false,
            }
        }

        case AuthActionTypes.STATUS_REQUEST: {
            return {
                ...state,
                isAuthenticated: false,
                error: null,
                loading: true,
              }
        }
        case AuthActionTypes.STATUS_SUCCESS: {
            return {
                ...state,
                isAuthenticated: true,
                user: {
                  token: action.payload.token,
                  email: action.payload.email,
                  _id: action.payload._id
                },
                error: null,
                loading: false,
              }
        }
        case AuthActionTypes.STATUS_FAILURE: {
            return {
                ...state,
                isAuthenticated: false,
                error: action.payload.error || 'error',
                loading: false,
              }
        }
        default: {
            return state
        }
    }
}
