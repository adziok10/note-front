// import {
//     createFeatureSelector,
//     createSelector,
//     MemoizedSelector
// } from '@ngrx/store'

// import { User } from '../../../modules/user/typings/User'

// import { authAdapter, State } from './state'

// export const getError = (state: State): any => state.error

// export const getIsLoading = (state: State): boolean => state.loading

// export const selectLoginState: MemoizedSelector<object, State> = createFeatureSelector<State>('login')

// export const selectLoginItems: (state: object) => User[] = authAdapter.getSelectors(selectLoginState).selectAll

// // export const selectMyFeatureById = (id: string) =>
// //     createSelector(this.selectLoginItems, (allMyFeatures: MyModel[]) => {
// //         if (allMyFeatures) {
// //             return allMyFeatures.find(p => p.id === id)
// //         } else {
// //             return null
// //         }
// //     })

// export const selectUserItems: (state: object) => User[] = authAdapter.getSelectors().selectAll

// export const selectLoginError: MemoizedSelector<object, any> = createSelector(
//     selectLoginState,
//     getError
// )

// export const selectLoginIsLoading: MemoizedSelector<object, boolean> = createSelector(selectMyFeatureState, getIsLoading)


import { createSelector } from '@ngrx/store'

import { User } from '../../../modules/user/typings/User'
import { RootState } from '../root-state'
import { AuthState } from './state'



export const user = (state: RootState) => state.authState

export const selectUser = createSelector(
    user,
    (state: User) => state
)

export const selectAuthUser = (state: AuthState) => state.user;
