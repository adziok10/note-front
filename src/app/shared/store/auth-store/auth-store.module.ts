import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { EffectsModule } from '@ngrx/effects'
import { StoreModule } from '@ngrx/store'

import { AuthEffects } from './effects'
import { authReducers } from './reducers'

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class LoginStoreModule { }

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature('auth', authReducers),
    EffectsModule.forFeature([AuthEffects])
  ],
  // providers: [AuthEffects]
})
export class AuthStoreModule {}
