import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity'

import { User } from '../../../modules/user/typings/User'

export const authAdapter: EntityAdapter<User> = createEntityAdapter<User>({
    selectId: model => model._id,
    sortComparer: (a: User, b: User): number =>
        b._id.toString().localeCompare(a._id.toString())
})

export interface AuthState extends EntityState<User> {
    isAuthenticated: boolean
    user: User | null
    error: string | null
    loading: boolean | null
}

export const initialState: AuthState = authAdapter.getInitialState({
    isAuthenticated: false,
    user: null,
    error: null,
    loading: false
})
