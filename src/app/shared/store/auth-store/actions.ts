import { Action } from '@ngrx/store'

import { User } from '../../../modules/user/typings/User'

export enum AuthActionTypes {
    LOGIN_REQUEST = '[Auth] LogIn Request',
    LOGIN_FAILURE = '[Auth] LogIn Failure',
    LOGIN_SUCCESS = '[Auth] LogIn Success',
    REGISTER_REQUEST = '[Auth] Register Request',
    REGISTER_FAILURE = '[Auth] Register Failure',
    REGISTER_SUCCESS = '[Auth] Register Success',
    LOGOUT = '[Auth] Logout',
    STATUS_REQUEST = '[Auth] Check Request',
    STATUS_FAILURE = '[Auth] Check Failure',
    STATUS_SUCCESS = '[Auth] Check Success',
}

export class LogInRequestAction implements Action {
    readonly type: string = AuthActionTypes.LOGIN_REQUEST

    constructor(public payload: any) { }
}

export class LogInFailureAction implements Action {
    readonly type: string = AuthActionTypes.LOGIN_FAILURE

    constructor(public payload: any) { }
}

export class LogInSuccessAction implements Action {
    readonly type: string = AuthActionTypes.LOGIN_SUCCESS

    constructor(public payload: any) { }
}

export class RegisterRequestAction implements Action {
    readonly type: string = AuthActionTypes.REGISTER_REQUEST

    constructor(public payload: any) { }
}

export class RegisterFailureAction implements Action {
    readonly type: string = AuthActionTypes.REGISTER_FAILURE

    constructor(public payload: any) { }
}

export class RegisterSuccessAction implements Action {
    readonly type: string = AuthActionTypes.REGISTER_SUCCESS

    constructor(public payload: any) { }
}
export class LogOutAction implements Action {
    readonly type: string = AuthActionTypes.LOGOUT

    constructor(public payload: any = {}) { }
}

export class StatusRequestAction implements Action {
    readonly type: string = AuthActionTypes.STATUS_REQUEST

    constructor(public payload: any = {}) { }
}

export class StatusFailureAction implements Action {
    readonly type: string = AuthActionTypes.STATUS_FAILURE

    constructor(public payload: any = {}) { }
}

export class StatusSuccessAction implements Action {
    readonly type: string = AuthActionTypes.STATUS_SUCCESS

    constructor(public payload: any = {}) { }
}

// tslint:disable-next-line: max-line-length
export type AuthAll = LogInRequestAction | LogInFailureAction | LogInSuccessAction | LogOutAction | StatusRequestAction | StatusFailureAction | StatusSuccessAction | RegisterFailureAction | RegisterRequestAction | RegisterSuccessAction

