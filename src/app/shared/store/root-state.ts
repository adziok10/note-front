import * as Auth from './auth-store/state'


export interface RootState {
  authState: Auth.AuthState
}
