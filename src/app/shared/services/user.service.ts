import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { environment } from '../../../environments/environment'

import { User } from '../../modules/user/typings/User'
import { Observable } from 'rxjs'

@Injectable({
    providedIn: 'root'
})
export class UserService {
    private url: string = `${environment.apiUrl}/user`

    getToken(): string {
        return localStorage.getItem('token')
    }

    constructor(private  http:  HttpClient) {}

    public logIn({ email, password }: { email: string, password: string }): Observable<User> {
        return this.http.post<User>(this.url, { email, password })
    }

    public register({ email, password, login }: { email: string, password: string, login: string }): Observable<User> {
      const url = `${this.url}/new`
      return this.http.post<User>(url, { email, password, login })
    }

    public checkStatus(): Observable<User> {
      const url = `${this.url}/status`
      return this.http.get<User>(url)
    }
}