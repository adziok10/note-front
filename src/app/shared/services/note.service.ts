import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'

import { environment } from '../../../environments/environment'
import { Note } from '../models/note'
import { Notebook } from '../models/Notebook';

@Injectable({
    providedIn: 'root'
})
export class NoteService {
    private url: string = `${environment.apiUrl}/note`

    getToken(): string {
        return localStorage.getItem('token')
    }

    constructor(private http:  HttpClient) {}

    public getNotes(_id: Note['_id']): Observable<Note[]> {
        return this.http.get<Note[]>(`${this.url}/list/${_id}`)
    }

    public getNote(_id: Note['_id']): Observable<Note> {
        return this.http.get<Note>(`${this.url}/${_id}`)
    }

    public createNote({ title, content, notebookId }: { title: string, content: string, notebookId: Notebook['_id'] }): Observable<Note> {
        return this.http.post<Note>(this.url, { title, content, notebookId })
    }

    public updateNote({ title, content, notebookId }: { title: string, content: string, notebookId: Notebook['_id'] }): Observable<Note> {
      const url = `${this.url}`
      return this.http.put<Note>(url, { title, content, notebookId })
    }

    public deleteNote(_id: Note['_id']): Observable<Note> {
        return this.http.delete<Note>(`${this.url}/${_id}`)
    }
}
