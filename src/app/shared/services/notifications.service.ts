import { Injectable } from '@angular/core'
import { Subject } from 'rxjs'

export interface Notification {
    msg: string
    delay: number
    cb?: Function
}

@Injectable({
    providedIn: 'root'
})
export class NotificationsService {
    public notifications: Subject<Notification> = new Subject()

    public show({ msg, delay }: Notification = { msg: '', delay: 5000 }, cb: Function = () => {}) {
        this.notifications.next({
            msg,
            delay,
            cb
        })
    }
}
