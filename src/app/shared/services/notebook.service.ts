import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'

import { environment } from '../../../environments/environment'
import { Notebook } from '../models/Notebook'

@Injectable({
    providedIn: 'root'
})
export class NotebookService {
    private url: string = `${environment.apiUrl}/notebook`

    getToken(): string {
        return localStorage.getItem('token')
    }

    constructor(private  http:  HttpClient) {}

    public getNotebooks(): Observable<Notebook[]> {
        return this.http.get<Notebook[]>(this.url)
    }

    public getNotebook(_id: string): Observable<Notebook> {
        return this.http.get<Notebook>(`${this.url}/${_id}`)
    }

    public createNotebook({ name, description, color }: { name: string, description: string, color: string }): Observable<Notebook> {
        return this.http.post<Notebook>(this.url, { name, description, color })
    }

    public updateNotebook({ name, description, color }: { name: string, description: string, color: string }): Observable<Notebook> {
      const url = `${this.url}/new`
      return this.http.put<Notebook>(url, { name, description, color })
    }

    public deleteNotebook(_id: string): Observable<Notebook> {
        return this.http.delete<Notebook>(`${this.url}/${_id}`)
    }
}