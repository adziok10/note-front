import { Component, OnInit, ComponentFactoryResolver, ComponentFactory, ComponentRef, ViewChild, ViewContainerRef } from '@angular/core'
import { Subscription } from 'rxjs'

import { NotificationsService, Notification } from '../../services/notifications.service'
import { NotificationItemComponent } from './notification-item/notification-item.component'

@Component({
    selector: 'root-notifications',
    templateUrl: './notifications.component.html',
    styleUrls: ['./notifications.component.sass']
})
export class NotificationsComponent implements OnInit {
    @ViewChild('wrapper', { read: ViewContainerRef, static: false }) container: ViewContainerRef

    private subs: Subscription = new Subscription()

    constructor(private notificationsService: NotificationsService, private resolver: ComponentFactoryResolver) { }

    ngOnInit() {
        this.subs.add(this.notificationsService.notifications.subscribe((notification: Notification) => {
            this.add(notification)
        }))
    }

    public hide() {

    }

    public add(notification: Notification) {
        const factory: ComponentFactory<NotificationItemComponent> = this.resolver.resolveComponentFactory(NotificationItemComponent)
        const component  = factory.create(null)

        component.instance.notification = notification

        this.container.insert(component.hostView, 99999)
    }
}
