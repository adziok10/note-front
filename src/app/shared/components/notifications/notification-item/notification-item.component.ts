import { Component, OnInit, Input, ElementRef } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { Notification } from '../../../services/notifications.service';

@Component({
    selector: 'root-notification-item',
    templateUrl: './notification-item.component.html',
    styleUrls: ['./notification-item.component.sass'],
})
export class NotificationItemComponent implements OnInit {
    @Input() notification: Notification

    public hide: boolean = false

    constructor(private elementRef: ElementRef) { }

    ngOnInit() {
        setTimeout(() => {
            this.notification.cb()
            this.hide = true
            this.elementRef.nativeElement.remove()
        }, this.notification.delay)
    }

}
