import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { NotificationsComponent } from './notifications/notifications.component';
import { NotificationItemComponent } from './notifications/notification-item/notification-item.component'

@NgModule({
  declarations: [
    NotificationsComponent,
    NotificationItemComponent
  ],
  exports: [
    NotificationsComponent
  ],
  imports: [
    CommonModule
  ]
})
export class ComponentsModule { }
