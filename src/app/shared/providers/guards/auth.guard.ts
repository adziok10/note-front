import { Injectable } from '@angular/core'
import { CanLoad, Route } from '@angular/router'
import { Observable } from 'rxjs'
import { Store, select } from '@ngrx/store'

import { RootState } from '../../store/root-state'
import { AuthState } from '../../store/auth-store/state'



@Injectable()
export class AuthGuard implements CanLoad {

    constructor(private store: Store<RootState>) {}

    canLoad(route: Route): Observable<boolean> | Promise<boolean> | boolean {
        // console.log('ds;jhsg;laidf')
        // this.store.select('authState').subscribe((data: State) => console.log(data));
        return !!this.store.pipe(select('isAuthenticated'))
    }

}
