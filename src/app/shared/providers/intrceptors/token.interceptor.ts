import { Injectable } from '@angular/core'
import {
    HttpEvent, HttpInterceptor, HttpHandler, HttpRequest
} from '@angular/common/http'

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
    intercept(req: HttpRequest<any>, next: HttpHandler) {
        const key = localStorage.getItem('token')

        const modified = req.clone({
            setHeaders: { 'Authorization': `Bearer ${key}` }
        })

        return next.handle(modified)
    }
}