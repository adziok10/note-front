import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { NotesRouter } from './notes.routes'

import { NotesPage } from './pages/notes/notes.page'
import { NotesNoteComponent } from './components/notes-note/notes-note.component'
import { NotesListComponent } from './components/notes-list/notes-list.component'

@NgModule({
    declarations: [
        NotesPage,
        NotesNoteComponent,
        NotesListComponent
    ],
    imports: [
        CommonModule,
        NotesRouter
    ]
})
export class NotesModule { }
