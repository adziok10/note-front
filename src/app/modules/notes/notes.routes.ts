import { ModuleWithProviders } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { NotesPage } from './pages/notes/notes.page'

const nootesRoutes: Routes = [
    {
        path: '',
        component: NotesPage
    },
    {
        path: ':id',
        component: NotesPage
    }
]

export const NotesRouter: ModuleWithProviders = RouterModule.forChild(nootesRoutes)
