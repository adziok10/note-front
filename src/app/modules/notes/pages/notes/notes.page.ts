import { Component, OnInit, OnDestroy } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { Subscription } from 'rxjs'

import { Note } from '../../../../shared/models/Note'

@Component({
    templateUrl: './notes.page.html',
    styleUrls: ['./notes.page.sass']
})
export class NotesPage implements OnInit, OnDestroy {
    public noteList: Note[] = []
    private subs: Subscription = new Subscription()

    constructor(private route: ActivatedRoute) { }

    ngOnInit() {
        this.subs.add(this.route.paramMap.subscribe(params => {
            console.log(params.get('id'))
        }))
    }

    ngOnDestroy() {
        this.subs.unsubscribe()
    }

}
