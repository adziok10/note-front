import { ModuleWithProviders } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { AuthGuard } from '../../shared/providers/guards/auth.guard'

import { HelloPage } from './pages/hello/hello.page'

const rootRoutes: Routes = [
    {
        path: '',
        component: HelloPage
    },
    // {
    //     path: 'user',
    //     loadChildren: () => import('../user/user.module').then(m => m.UserModule),
    //     canLoad: [AuthGuard],
    // },
    {
        path: 'notes',
        loadChildren: () => import('../notes/notes.module').then(m => m.NotesModule),
        canLoad: [AuthGuard],
    },
]

export const RootRouter: ModuleWithProviders = RouterModule.forRoot(rootRoutes)
