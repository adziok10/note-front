import { NgModule } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { BrowserModule } from '@angular/platform-browser'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'

import { MaterialModule } from '../../shared/material.module'
import { RootStoreModule } from '../../shared/store/root-store.module'
import { ComponentsModule } from '../../shared/components/components.module'

import { AuthGuard } from '../../shared/providers/guards/auth.guard'
import { RootRouter } from '../../modules/root/root.routes'
import { TokenInterceptor } from '../../shared/providers/intrceptors/token.interceptor'

import { NotificationsService } from '../../shared/services/notifications.service'

import { HelloPage } from './pages/hello/hello.page'
import { DefaultPage } from './pages/default/default.page'

import { RootHeaderComponent } from './components/root-header/root-header.component'
import { RootNotebookListComponent } from './components/root-notebook-list/root-notebook-list.component'

import { RootNootebookCreateDialog } from './components/root-nootebook-create/root-nootebook-create.dialog'

import { BootstrapHelper } from '../../shared/utils/bootstrap.util'
import { NotificationItemComponent } from '../../shared/components/notifications/notification-item/notification-item.component'

@NgModule({
    declarations: [
        HelloPage,
        DefaultPage,
        RootHeaderComponent,
        RootNotebookListComponent,
        RootNootebookCreateDialog,
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        RootRouter,
        BrowserAnimationsModule,
        RootStoreModule,
        MaterialModule,
        FormsModule,
        ComponentsModule,
    ],
    providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: TokenInterceptor,
          multi: true
        },
        AuthGuard,
        BootstrapHelper,
        NotificationsService,
    ],
    bootstrap: [DefaultPage],
    entryComponents: [RootNootebookCreateDialog, NotificationItemComponent]
})
export class RootModule { }
