import { Component, OnInit } from '@angular/core'

@Component({
  selector: 'root-header',
  templateUrl: './root-header.component.html',
  styleUrls: ['./root-header.component.sass']
})
export class RootHeaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
