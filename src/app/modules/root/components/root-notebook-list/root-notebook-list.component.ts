import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { MatDialog } from '@angular/material/dialog'

import { RootNootebookCreateDialog } from '../root-nootebook-create/root-nootebook-create.dialog'
import { NotebookService } from '../../../../shared/services/notebook.service'
import { Notebook } from '../../../../shared/models/Notebook'
import { NotificationsService } from '../../../../shared/services/notifications.service'

@Component({
  selector: 'root-notebook-list',
  templateUrl: './root-notebook-list.component.html',
  styleUrls: ['./root-notebook-list.component.sass']
})
export class RootNotebookListComponent implements OnInit {
    public notebookList: Notebook[] = []

    public isLoading: boolean = true


    constructor(public dialog: MatDialog,
        private notebookService: NotebookService,
        private notificationsService: NotificationsService,
        private router: Router) {}

    ngOnInit() {
        this.notebookService.getNotebooks().subscribe((notebooks) => {
            this.notebookList = notebooks
            this.isLoading = false
        })
    }

    public openDialog(): void {
        const dialogRef = this.dialog.open(RootNootebookCreateDialog, {
            width: '450px',
            data: { name: '', description: '' }
        })

        dialogRef.afterClosed().subscribe(result => {
            this.notebookService.createNotebook(result).subscribe((e) => {
                this.notificationsService.show({ msg: result.name, delay: 5000 })
            })
        })
    }

    public loadNotebook(id: Notebook['_id']){
        this.router.navigate(['/notes', id])
    }

}
