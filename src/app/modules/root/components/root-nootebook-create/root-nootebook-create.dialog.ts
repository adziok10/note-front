import { Component, OnInit, Inject } from '@angular/core'
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog'

@Component({
  selector: 'root-nootebook-create',
  templateUrl: './root-nootebook-create.dialog.html',
  styleUrls: ['./root-nootebook-create.dialog.sass']
})
export class RootNootebookCreateDialog implements OnInit {

    constructor(
        public dialogRef: MatDialogRef<RootNootebookCreateDialog>,
        @Inject(MAT_DIALOG_DATA) public notebook: any
        ) {}

    ngOnInit() {}

    onNoClick(): void {
        this.dialogRef.close()
    }
}
