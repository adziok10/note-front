import { Component, OnInit } from '@angular/core'
import { Store } from '@ngrx/store'

import { RootState } from '../../../../shared/store/root-state'
import { AuthState } from '../../../../shared/store/auth-store/state'
import { StatusRequestAction, LogInRequestAction, LogOutAction, RegisterRequestAction } from '../../../../shared/store/auth-store/actions'
import { User } from '../../../user/typings/User';

@Component({
    selector: 'app-root',
    templateUrl: './default.page.html',
    styleUrls: ['./default.page.sass']
})
export class DefaultPage implements OnInit {
    public register: boolean = false

    public user: any = {
        email: '',
        password: '',
        login: ''
    }

    public authData: AuthState

    constructor(private store: Store<RootState>) { }

    ngOnInit() {
        this.initializeApp()
        this.store.select('auth').subscribe((data: AuthState) => this.authData = data)
    }

    private initializeApp() {
        this.store.dispatch(new StatusRequestAction())
    }

    public logIn() {
        this.store.dispatch(new LogInRequestAction({ email: this.user.email, password: this.user.password }))
    }

    public singUp() {
        this.store.dispatch(new RegisterRequestAction({ email: this.user.email, password: this.user.password, login: this.user.login }))
    }

    public logOut() {
        this.store.dispatch(new LogOutAction())
    }

}
