import { ModuleWithProviders } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { LoginPage } from './pages/login/login.page'

const rootRoutes: Routes = [
    {
        path: '',
        component: LoginPage,
    },
    // {
    //     path: 'register',
    //     component: LoginPage
    // }
]

export const UserRouter: ModuleWithProviders = RouterModule.forChild(rootRoutes)
