import { NgModule } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { CommonModule } from '@angular/common'

import { UserRouter } from './user.routes'

import { LoginPage } from './pages/login/login.page'

@NgModule({
    declarations: [
        LoginPage
    ],
    imports: [
        CommonModule,
        FormsModule,
        UserRouter
    ]
})
export class UserModule { }
