import { Component, OnInit } from '@angular/core'
import { Store } from '@ngrx/store'

import { LogInRequestAction, LogOutAction } from '../../../../shared/store/auth-store/actions'
import { RootState } from '../../../../shared/store/root-state'

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.sass']
})
export class LoginPage implements OnInit {

    public user: any = {
        email: '',
        password: ''
    }

    constructor(private store: Store<RootState>) { }

    ngOnInit() {
        console.log('elo')
    }

    public onSubmit() {
        this.store.dispatch(new LogInRequestAction({ email: this.user.email, password: this.user.password }))
    }

    public logOut() {
        this.store.dispatch(new LogOutAction())
    }
}
