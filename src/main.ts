import 'hammerjs';
import { enableProdMode } from '@angular/core'
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic'

import { RootModule } from './app/modules/root/root.module'
import { environment } from './environments/environment'
import { hmrBootstrap } from './hmr'

if (environment.production) {
  enableProdMode()

  if (window) {
      window.console.log = () => {}
  }
}

const bootstrap = () => platformBrowserDynamic().bootstrapModule(RootModule)

if (environment.hmr) {
  if (module['hot']) {
    hmrBootstrap(module, bootstrap)
  } else {
    console.error('HMR is not enabled for webpack-dev-server!')
    console.log('Are you using the --hmr flag for ng serve?')
  }
} else {
  bootstrap().catch(err => console.log(err))
}
